README
---------------------------
A module that lists every user on the site with a certain profile_field filled in, similar to a site directory of users with that information listed (i.e., users with an AOL Instant Messenger or XFire account)


Installation
---------------------------
After you have installed the module through the standard Drupal 5.x workflow, you will want to add a link to the profile pages from your user's profiles.  You can do so by overriding theme_user_profile in your theme's template.php file.  See below for a sample.

All profile pages will be accessible via the URL scheme http://example.com/profile_pages/profile_field_name.

Visiting http://yoursite.com/profile_pages will list all profile fields with a link to their respective profile page.


Sample theme function
---------------------------
Notice the link created with the text 'View All':

function example_user_profile($account, $fields) {
  $output = '<div class="profile">';
  $output .= theme('user_picture', $account);
  foreach ($fields as $category => $items) {
    if (strlen($category) > 0) {
      $output .= '<h2 class="title">'. check_plain($category) .'</h2>';
    }
    $output .= '<dl>';
    foreach ($items as $item) {
      if (isset($item['title'])) {
        $output .= '<dt class="'. $item['class'] .'">'. $item['title'] .'</dt>';
      }
      $output .= '<dd class="'. $item['class'] .'">'. $item['value'] . ' <strong>' . l(t('(View All)'), 'profile_pages/'.$type) . '</strong></dd>';
    }
    $output .= '</dl>';
  }
  $output .= '</div>';

  return $output;
}